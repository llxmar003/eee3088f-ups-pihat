Before opening the Voltage Regulator Circuit in LTSpice, the spice model for LM2678 must be added to avoid error messages on startup.
This model can be found in the LM2678_PSPICE_TRANS directory within the current directory (Simulations).

1) Open LTSpice and drag and drop the LM2678_TRANS.lib file into LTSpice
2) Right click the line in blue beginning in ".SUBCKT LM2678" and click "Create Symbol" in the pop-up menu
3) Accept the pop-up window
4) You should now be able to open the schematic with no errors 
