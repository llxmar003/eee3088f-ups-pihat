EEE3088F UPS PiHat for the Raspberry Pi Zero

This is a battery pack HAT for the Raspberry Pi Zero, which allows for the Pi Zero to continue operation in the event of a blackout. This would allow the system to continue running without external power for the duration of the blackout and/or to allow for a safe shutdown without hardware failure or data loss. In the event of power failure, the HAT will switch power sources smoothly without requiring a shutdown of the device.

How to use this HAT:

Operation of this Hat requires that it be the only power source for your Pi. As such operation of this HAT assumes that you have connected this UPS HAT to a 5V regulated input via the USB C port on the HAT PCB. The HAT uses this input to firstly detect power outages and secondly to power your Raspberry Pi from the regulated input while power is still available.

DO NOT CONNECT YOUR PI DIRECTLY TO POWER VIA ANY OTHER INPUT CONNECTIONS - this will render the UPS useless as it will not be able to detect if the Pi is recieving power from the UPS' external batteries or from a regulated external input. As such the HAT may push power from its own supply to the Pi while the Pi is recieving external power which may cause damage to both the Pi and the HAT's circuitry. 

This UPS provides additional functionality on the GPIO24 Pin. When the battery voltage is low, the GPIO pin will pull high. This provides the user with the ability to program a personalised low battery response, via programming a notification or auto-shutdown procedure. Alternatively users cans make use of the following code which will set the default low battery behaviour to be a shutdown:

(Imagine the code is written here)

The UPS's power source requires three 18650 Lithium Ion batteries (not included). A holder such as this one (https://www.robotics.org.za/18650-WIRE-3X) is reccommded. Additionally the HAT's PCB has a 2.1mm Male Barrel Jack on it to connect to the external batery supply and hence the battery holder will require the 2.1mm Female counter part for the Barrel Jack, one can be found here (http://archive.communica.co.za/Catalog/Details/P1636813917).

In order to conserve energy the HAT has a push button to connect the Battery Indicator LED's to view battery voltage status, if pushed and only the red LED lights up, it is reccommended that the batteries be charged at the earliest convenience of the user as running the batteries past this point may cause permanent damage to the batteries. 

Regarding Manufacturing: 

The BOM (bill of materials) may be found here (https://gitlab.com/llxmar003/eee3088f-ups-pihat/-/blob/master/PCB/UPS-uHAT_Compiled_PCB/UPS-uHAT_BOM.xml). Note the file is an XML file and it is reccommded to open it using MS Excel. 

If users want to manufacture this PCB for themselves, the relevant Gerber and Drill files can be found here (https://gitlab.com/llxmar003/eee3088f-ups-pihat/-/tree/master/PCB/UPS-uHAT_Compiled_PCB/UPS_Compiled_Gerber_And_Drill_Files). The whole folder can be compressed/zipped and sent to your manufacturer of choice as they are. 

Alternatively if users wish make changes to the KiCad Schematic and/or PCB, new Gerber files will have to be generated. The process for this outlined below:

Changes in schematic file: 
1. Make changes to schematic
2. Click "Annotate schematic"
3. Click "Perform Electrical Rules Check" 
4. Click "Assign PCB footprints to symbols" 
5. Click "Generate netlist"

Move to PCB file for the these changes:

6. Click "Update PCB from Schematic"
7. Make other changes to PCB as desired 
8. Click "Perform Design Rules Check" and double check on warnings (cosmetic warnings may be ignored if they do not affect functionality)

Generating Drill and Gerber Files:

9. Click the "Plot" button and select "Gerber"
10. Click "Generate Drill Files..." then in the pop up menu first click "Generate Map Files" then "Generate Drill Files"
11. Click "Plot" 

The Gerber and Drill files may be found in the same directory that the project lies in. 
