EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7050 6800 0    50   ~ 0
SMXCHR002\nChristal Sima
Text Notes 7400 7500 0    50   ~ 0
UPS uHAT Amplification circuit
Text Notes 8150 7650 0    50   ~ 0
03/06/2021
Text Notes 10600 7650 0    50   ~ 0
01
$Comp
L Amplifier_Operational:LM741 U?
U 1 1 60B90C8B
P 5150 3500
F 0 "U?" H 5350 3400 50  0000 L CNN
F 1 "LM741" H 5300 3300 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 5200 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 5300 3650 50  0001 C CNN
	1    5150 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60B92C93
P 4550 3450
F 0 "R?" H 4618 3496 50  0000 L CNN
F 1 "1k" H 4618 3405 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" H 4550 3450 50  0001 C CNN
F 3 "~" H 4550 3450 50  0001 C CNN
	1    4550 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60B9322B
P 4550 3750
F 0 "R?" H 4618 3796 50  0000 L CNN
F 1 "1k" H 4618 3705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" H 4550 3750 50  0001 C CNN
F 3 "~" H 4550 3750 50  0001 C CNN
	1    4550 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60B93347
P 5650 3600
F 0 "R?" H 5718 3646 50  0000 L CNN
F 1 "1k" H 5718 3555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" H 5650 3600 50  0001 C CNN
F 3 "~" H 5650 3600 50  0001 C CNN
	1    5650 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D?
U 1 1 60B938A3
P 5650 3900
F 0 "D?" V 5604 3979 50  0000 L CNN
F 1 "3V3" V 5695 3979 50  0000 L CNN
F 2 "Diode_THT:D_A-405_P10.16mm_Horizontal" H 5650 3900 50  0001 C CNN
F 3 "~" H 5650 3900 50  0001 C CNN
	1    5650 3900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B93F4F
P 5650 4050
F 0 "#PWR?" H 5650 3800 50  0001 C CNN
F 1 "GND" H 5655 3877 50  0000 C CNN
F 2 "" H 5650 4050 50  0001 C CNN
F 3 "" H 5650 4050 50  0001 C CNN
	1    5650 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B94276
P 5050 4050
F 0 "#PWR?" H 5050 3800 50  0001 C CNN
F 1 "GND" H 5055 3877 50  0000 C CNN
F 2 "" H 5050 4050 50  0001 C CNN
F 3 "" H 5050 4050 50  0001 C CNN
	1    5050 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B946C7
P 4550 4050
F 0 "#PWR?" H 4550 3800 50  0001 C CNN
F 1 "GND" H 4555 3877 50  0000 C CNN
F 2 "" H 4550 4050 50  0001 C CNN
F 3 "" H 4550 4050 50  0001 C CNN
	1    4550 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60B94C34
P 4550 3950
F 0 "R?" H 4618 3996 50  0000 L CNN
F 1 "100" H 4618 3905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" H 4550 3950 50  0001 C CNN
F 3 "~" H 4550 3950 50  0001 C CNN
	1    4550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3700 5650 3750
Wire Wire Line
	5650 3500 5450 3500
Wire Wire Line
	5050 4050 5050 3800
Wire Wire Line
	4550 3600 4850 3600
Wire Wire Line
	4550 3600 4550 3550
Wire Wire Line
	4550 3600 4550 3650
Connection ~ 4550 3600
Wire Wire Line
	4550 3200 4550 3350
NoConn ~ 5150 3800
NoConn ~ 5250 3800
Text Label 5050 3200 0    50   ~ 0
Vreg
Text Label 4550 3200 2    50   ~ 0
Vbatt
Text Label 4850 3200 2    50   ~ 0
Vreg
Wire Wire Line
	4850 3200 4850 3400
Wire Wire Line
	5850 3750 5650 3750
Connection ~ 5650 3750
Text Label 5850 3750 0    50   ~ 0
Vref
$EndSCHEMATC
