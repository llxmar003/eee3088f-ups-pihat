EESchema Schematic File Version 4
LIBS:RPi_Zero_pHat_Template-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7050 6800 0    50   ~ 0
SMXCHR002\nChristal Sima
Text Notes 7400 7500 0    50   ~ 0
UPS uHAT Ports and supply detection circuit
Text Notes 10600 7650 0    50   ~ 0
01\n
Text Notes 8150 7650 0    50   ~ 0
01/06/2021
$Comp
L Device:R_Small_US R?
U 1 1 60B2374E
P 4000 3700
F 0 "R?" H 4068 3746 50  0000 L CNN
F 1 "1k" H 4068 3655 50  0000 L CNN
F 2 "" H 4000 3700 50  0001 C CNN
F 3 "~" H 4000 3700 50  0001 C CNN
	1    4000 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60B2435B
P 3600 3700
F 0 "R?" H 3668 3746 50  0000 L CNN
F 1 "20k" H 3668 3655 50  0000 L CNN
F 2 "" H 3600 3700 50  0001 C CNN
F 3 "~" H 3600 3700 50  0001 C CNN
	1    3600 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60B24F26
P 4500 4300
F 0 "R?" H 4568 4346 50  0000 L CNN
F 1 "100k" H 4568 4255 50  0000 L CNN
F 2 "" H 4500 4300 50  0001 C CNN
F 3 "~" H 4500 4300 50  0001 C CNN
	1    4500 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q?
U 1 1 60B25ED0
P 3900 4100
F 0 "Q?" H 4091 4146 50  0000 L CNN
F 1 "SST2222A" H 4091 4055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4100 4200 50  0001 C CNN
F 3 "https://datasheetspdf.com/pdf-file/830453/Rohm/SST2222A/1" H 3900 4100 50  0001 C CNN
	1    3900 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q?
U 1 1 60B26DBB
P 4400 3850
F 0 "Q?" H 4591 3896 50  0000 L CNN
F 1 "SST2222A" H 4591 3805 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4600 3950 50  0001 C CNN
F 3 "https://datasheetspdf.com/pdf-file/830453/Rohm/SST2222A/1" H 4400 3850 50  0001 C CNN
	1    4400 3850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B323D0
P 4500 4500
F 0 "#PWR?" H 4500 4250 50  0001 C CNN
F 1 "GND" H 4505 4327 50  0000 C CNN
F 2 "" H 4500 4500 50  0001 C CNN
F 3 "" H 4500 4500 50  0001 C CNN
	1    4500 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B33698
P 4000 4500
F 0 "#PWR?" H 4000 4250 50  0001 C CNN
F 1 "GND" H 4005 4327 50  0000 C CNN
F 2 "" H 4000 4500 50  0001 C CNN
F 3 "" H 4000 4500 50  0001 C CNN
	1    4000 4500
	1    0    0    -1  
$EndComp
Text Label 5200 4100 2    50   ~ 0
Switch
Text Label 5200 4200 2    50   ~ 0
PowerStatusLED
NoConn ~ 6950 5000
NoConn ~ 6950 4900
NoConn ~ 6950 4600
NoConn ~ 6950 4500
NoConn ~ 6950 4400
NoConn ~ 6950 4300
NoConn ~ 6950 4100
NoConn ~ 6950 4000
Text Label 6950 3800 0    50   ~ 0
Vsupp
Text Label 7200 3100 0    50   ~ 0
Vbatt
$Comp
L power:GND #PWR?
U 1 1 60B90D35
P 7500 3250
F 0 "#PWR?" H 7500 3000 50  0001 C CNN
F 1 "GND" H 7505 3077 50  0000 C CNN
F 2 "" H 7500 3250 50  0001 C CNN
F 3 "" H 7500 3250 50  0001 C CNN
	1    7500 3250
	1    0    0    -1  
$EndComp
Text Label 3600 3600 1    50   ~ 0
Vsupp
Text Label 4000 3500 1    50   ~ 0
Vbatt
Text Label 4500 3500 1    50   ~ 0
Vbatt
Text Notes 7065 6715 0    50   ~ 0
NDXBRE004\nBrennan Naidoo\n
Text Notes 10575 7630 0    50   ~ 0
1.0\n
Text Notes 8145 7715 0    50   ~ 0
31/05/2021\n\n
Text Notes 7325 7575 0    50   ~ 0
UPS uHat LED Battery detection circuit\n\n
Wire Wire Line
	4000 3500 4000 3600
Wire Wire Line
	4000 3800 4000 3850
Wire Wire Line
	4500 3500 4500 3650
Wire Wire Line
	4200 3850 4000 3850
Connection ~ 4000 3850
Wire Wire Line
	4000 3850 4000 3900
Wire Wire Line
	3600 3800 3600 4100
Wire Wire Line
	3600 4100 3700 4100
Wire Wire Line
	4000 4300 4000 4500
Wire Wire Line
	4500 4400 4500 4500
Wire Wire Line
	4500 4050 4500 4100
Wire Wire Line
	5200 4200 4600 4200
Wire Wire Line
	4600 4200 4600 4100
Wire Wire Line
	4600 4100 4500 4100
Connection ~ 4500 4100
Wire Wire Line
	4500 4100 4500 4200
Wire Wire Line
	5200 4100 4600 4100
Connection ~ 4600 4100
Wire Wire Line
	7500 3250 7500 2900
Wire Wire Line
	7500 2900 6950 2900
NoConn ~ 6050 5300
$Comp
L power:GND #PWR?
U 1 1 60B63465
P 6350 5300
F 0 "#PWR?" H 6350 5050 50  0001 C CNN
F 1 "GND" H 6355 5127 50  0000 C CNN
F 2 "" H 6350 5300 50  0001 C CNN
F 3 "" H 6350 5300 50  0001 C CNN
	1    6350 5300
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_C_Receptacle_USB2.0 J?
U 1 1 60B1F750
P 6350 4400
F 0 "J?" H 6457 5267 50  0000 C CNN
F 1 "USB_C_Receptacle_USB2.0" H 6457 5176 50  0000 C CNN
F 2 "Connector_USB:USB_C_Receptacle_GCT_USB4085" H 6500 4400 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 6500 4400 50  0001 C CNN
	1    6350 4400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Barrel_Jack J?
U 1 1 60B9D336
P 6650 3000
F 0 "J?" H 6707 3325 50  0000 C CNN
F 1 "Barrel_Jack" H 6707 3234 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-063AH_Horizontal" H 6700 2960 50  0001 C CNN
F 3 "https://www.cuidevices.com/product/resource/pj-063ah.pdf" H 6700 2960 50  0001 C CNN
	1    6650 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3100 7200 3100
$EndSCHEMATC
