EESchema Schematic File Version 4
LIBS:LED_Supply_Status_Circuit-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small_US R?
U 1 1 60B8FE9C
P 2550 5950
F 0 "R?" H 2618 5996 50  0000 L CNN
F 1 "330" H 2618 5905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" H 2550 5950 50  0001 C CNN
F 3 "~" H 2550 5950 50  0001 C CNN
	1    2550 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 60B90188
P 2550 6450
F 0 "D?" V 2589 6333 50  0000 R CNN
F 1 "LED" V 2498 6333 50  0000 R CNN
F 2 "LED_SMD:LED_2816_7142Metric_Pad3.20x4.45mm_HandSolder" H 2550 6450 50  0001 C CNN
F 3 "~" H 2550 6450 50  0001 C CNN
	1    2550 6450
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60B90A9E
P 2550 6900
F 0 "#PWR?" H 2550 6650 50  0001 C CNN
F 1 "GND" H 2555 6727 50  0000 C CNN
F 2 "" H 2550 6900 50  0001 C CNN
F 3 "" H 2550 6900 50  0001 C CNN
	1    2550 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 6600 2550 6900
Wire Wire Line
	2550 6300 2550 6050
Wire Wire Line
	2550 5650 2550 5850
Text Label 2550 5650 0    50   ~ 0
PowerStatusLED
Text Notes 7100 6700 0    50   ~ 0
SMXCHR002
Text Notes 7100 6800 0    50   ~ 0
Christal Sima
Text Notes 10600 7650 0    50   ~ 0
01
Text Notes 8150 7650 0    50   ~ 0
03/06/2021
Text Notes 7400 7500 0    50   ~ 0
UPS uHAT LED Supply Detection Circuit
$EndSCHEMATC
