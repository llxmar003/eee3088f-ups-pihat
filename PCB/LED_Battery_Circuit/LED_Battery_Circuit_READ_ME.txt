How this circuit works:

As the Hat’s battery supply voltage decreases over time, the LED’s in the circuit function as indicators of the amount of power left in the battery. Each branch in the circuit has a voltage level whereby if the battery voltage drops below it, the zener diode in the branch will no longer be reverse biased, causing the LED to turn off. 

LED meanings:

The colour of each LED serves as a visual indicator for the battery’s voltage. The green LED indicates a high battery voltage and should be on until the battery voltage drops to 12V, the yellow LED indicates a medium battery voltage and should be on until the battery voltage drops to 11.1V. The red LED indicates a low battery voltage - i.e. the battery requires charging soon, and should stay on until the battery is completely drained at 10.2V. 
